{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wincomplete-patterns #-}
{-# OPTIONS_GHC -Wno-unused-top-binds #-}

module Main (main) where

import Codec.Compression.Zlib (compress, decompress)
import Control.Arrow (second)
import Crypto.Hash (Digest, SHA1, hashlazy)
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as C8
import Data.List (intercalate, unfoldr)
import Data.Maybe (fromJust)
import Data.String (fromString)
import Data.Word (Word8)
import Options.Applicative
import System.Directory (createDirectoryIfMissing)
import System.FilePath ((</>))
import System.IO (IOMode (WriteMode), hPutStrLn, stderr, withFile)

objectsDir :: FilePath
objectsDir = ".git/objects"

nullByte, spaceByte :: Word8
nullByte = 0
spaceByte = 32

data Command
  = Init
  | CatFile Bool Sha
  | HashObject Bool FilePath
  | LsTree Bool Sha

type Sha = String

data TreeItem
  = TreeItem String String Sha

main :: IO ()
main = do
  hPutStrLn stderr "Logs from your program will appear here"

  execParser optsParser >>= runCommand
  where
    optsParser :: ParserInfo Command
    optsParser =
      info
        (helper <*> programOptions)
        ( fullDesc
            <> progDesc "Git clone"
            <> header "git-clone - a CodeCrafters clone of Git"
        )
    programOptions :: Parser Command
    programOptions =
      hsubparser (initCommand <> catFileCommand <> hashObjectCommand <> lsTreeCommand)
    initCommand :: Mod CommandFields Command
    initCommand =
      command
        "init"
        (info (pure Init) (progDesc "Initialise the Git repo"))
    catFileCommand :: Mod CommandFields Command
    catFileCommand =
      command
        "cat-file"
        (info catFileOptions (progDesc "Cat the thing"))
    catFileOptions :: Parser Command
    catFileOptions =
      CatFile
        <$> switch (short 'p' <> help "Whether to print")
        <*> strArgument (metavar "SHA" <> help "Name of the thing to cat")
    hashObjectCommand :: Mod CommandFields Command
    hashObjectCommand =
      command
        "hash-object"
        (info hashObjectOptions (progDesc "Hash an object"))
    hashObjectOptions :: Parser Command
    hashObjectOptions =
      HashObject
        <$> switch (short 'w' <> help "Whether to write to objects directory")
        <*> strArgument (metavar "FILE" <> help "Name of the file to hash")
    lsTreeCommand :: Mod CommandFields Command
    lsTreeCommand =
      command
        "ls-tree"
        (info lsTreeOptions (progDesc "List tree objects"))
    lsTreeOptions :: Parser Command
    lsTreeOptions =
      LsTree
        <$> switch (long "name-only" <> short 'n' <> help "Whether to output object names only")
        <*> strArgument (metavar "SHA" <> help "Hash of the tree to list")

runCommand :: Command -> IO ()
runCommand Init = do
  createDirectoryIfMissing createParents ".git"
  createDirectoryIfMissing createParents (".git" </> "objects")
  createDirectoryIfMissing createParents (".git" </> "refs")
  writeFile (".git" </> "HEAD") "ref: refs/heads/main\n"
  putStrLn "Initialized git directory"
  where
    createParents = True
runCommand (CatFile _ hash) = do
  decompressed <- decompress <$> B.readFile (objectsDir </> folder </> file)
  let (header, content) = splitOn nullByte decompressed
      [objectType, sizeBytes] = B.split spaceByte header
      size = fromIntegral . fst . fromJust $ C8.readInt sizeBytes
  case objectType of
    "blob" -> B.putStr . B.take size $ content
    _ -> error "Not a blob!"
  where
    (folder, file) = folderAndFile hash
runCommand (HashObject write file) = do
  content <- B.readFile file
  let output = "blob " <> (fromString . show . B.length) content <> "\0" <> content
      sha :: Digest SHA1
      sha = hashlazy output
      (folder, file) = folderAndFile (show sha)
  createDirectoryIfMissing createParents (objectsDir </> folder)
  B.writeFile (objectsDir </> folder </> file) (compress output)
  print sha
  where
    createParents = True
runCommand (LsTree printNameOnly hash) = do
  decompressed <- decompress <$> B.readFile (objectsDir </> folder </> file)
  let (header, content) = splitOn nullByte decompressed
      [objectType, sizeBytes] = B.split spaceByte header
      size = fromIntegral . fst . fromJust $ C8.readInt sizeBytes
  case objectType of
    "tree" -> putStrLn . intercalate "\LF" . map itemDetails $ unfoldr parseTreeItem content
    _ -> error "Not a tree!"
  where
    (folder, file) = folderAndFile hash
    itemDetails (TreeItem mode name hash) =
      if printNameOnly
        then name
        else mode <> " " <> name <> " " <> hash

folderAndFile :: Sha -> (String, String)
folderAndFile = splitAt 2

parseTreeItem :: B.ByteString -> Maybe (TreeItem, B.ByteString)
parseTreeItem s =
  if B.null s
    then Nothing
    else
      let (header, rest) = splitOn nullByte s
          (mode, name) = splitOn spaceByte header
          (sha, unparsed) = B.splitAt hashLength rest
       in Just (TreeItem (C8.unpack mode) (C8.unpack name) (C8.unpack sha), unparsed)
  where
    hashLength = 20

splitOn :: Word8 -> B.ByteString -> (B.ByteString, B.ByteString)
splitOn w s = second (B.drop 1) $ B.break (== w) s